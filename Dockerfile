FROM php:8.1-apache

# Enable Apache modules
RUN a2enmod rewrite

# Copy project files to container
COPY src/ /var/www/html/

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN docker-php-ext-install mysqli pdo_mysql
RUN apt-get update && apt-get install -y zip unzip git

# Install project dependencies
WORKDIR /var/www/html/

EXPOSE 80