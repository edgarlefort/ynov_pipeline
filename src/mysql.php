<?php

require_once __DIR__ . '/vendor/autoload.php'; // Autoload files using Composer autoload

use Aws\CloudWatchLogs\CloudWatchLogsClient;
use Maxbanton\Cwh\Handler\CloudWatch;
use Monolog\Logger;
use Monolog\Formatter\JsonFormatter;

$bdd_params_object = new stdClass;
$bdd_params_object->db_host = "ynov.csiyxrdebz9x.eu-north-1.rds.amazonaws.com";
$bdd_params_object->db_name = "ynov";
$bdd_params_object->db_port = getenv('DB_PORT') ?: 3306;
$bdd_params_object->user = "admin";
$bdd_params_object->password = "tE9KCmd!";
$bdd_params_object->connexion = 'host=';
$bdd_params_object->charset = 'utf8';

try {
    $bdd = new PDO('mysql:' . $bdd_params_object->connexion . $bdd_params_object->db_host . ';dbname=' . $bdd_params_object->db_name . ';charset=' . $bdd_params_object->charset, $bdd_params_object->user, $bdd_params_object->password);
} catch (Exception $e) {
    echo "ERREUR CONNEXION BDD";
    echo '<pre>';
    print_r($bdd_params_object);
    echo '</pre>';
    die('Erreur : ' . $e->getMessage());
}

$req = $bdd->prepare('SELECT * FROM users');
$req->execute();
var_dump($req->fetchAll());

$sdkParams = [
    'region' => getenv('AWS_DEFAULT_REGION', 'us-east-1'),
    'version' => 'latest',
    'credentials' => [
        'key' => getenv('AWS_ACCESS_KEY_ID'),
        'secret' => getenv('AWS_SECRET_ACCESS_KEY'),
    ]
];
// 'token' => 'your AWS session token', // token is optional

// Instantiate AWS SDK CloudWatch Logs Client
$client = new CloudWatchLogsClient($sdkParams);

// Log group name, will be created if none
$groupName = 'php-logtest';

// Log stream name, will be created if none
$streamName = 'ecs-stream-ynov';

// Days to keep logs, 14 by default. Set to `null` to allow indefinite retention.
$retentionDays = 30;

// Instantiate handler (tags are optional)
$handler = new CloudWatch($client, $groupName, $streamName, $retentionDays, 10000, ['my-awesome-tag' => 'tag-value']);

// Optionally set the JsonFormatter to be able to access your log messages in a structured way
$handler->setFormatter(new JsonFormatter());

// Create a log channel
$log = new Logger("name");

// Set handler
$log->pushHandler($handler);

// Add records to the log
$log->debug('Connected to MySQL database and fetched all users', ['users' => $req->fetchAll()]);