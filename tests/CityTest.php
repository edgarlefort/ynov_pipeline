<?php

require_once './src/vendor/autoload.php'; // Autoload files using Composer autoload
include_once('./src/City.php');

class CityTest extends \PHPUnit\Framework\TestCase
{
    public function testgetCityNameById()
    {
        $city = new City();
        $result = $city->getCityNameById(1);
        $expected = 'Bordeaux';
        $this->assertTrue($result == $expected);
    }
}
